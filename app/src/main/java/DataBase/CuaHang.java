package DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class CuaHang extends SQLiteOpenHelper {
    public CuaHang(@Nullable Context context) {
        super(context, "CuaHang", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String sql1 = "CREATE TABLE dsCuahang(id INTEGER PRIMARY KEY AUTOINCREMENT, anh text, ten text, mota text, diachi text, mucgia text)";
        sqLiteDatabase.execSQL(sql1);
        sql1 = "INSERT INTO dsCuahang(anh, ten, mota, diachi, mucgia) VALUES('ch1','Highland Coffee','Nhiều món mới','20 Lê Lợi, Hải Châu','25.000 đồng - 35.000 đồng')";
        sqLiteDatabase.execSQL(sql1);
        sql1 = "INSERT INTO dsCuahang(anh, ten, mota, diachi, mucgia) VALUES('ch2','The Alley ','Sẽ quay lại ủng hộ','130 Đống Đa, Hải Châu,','55.000 đồng - 100.000 đồng')";
        sqLiteDatabase.execSQL(sql1);
        sql1 = "INSERT INTO dsCuahang(anh, ten, mota, diachi, mucgia) VALUES('ch3','Ẩm thực Gánh - Bún đậu mắm tôm','Sẽ quay lại ủng hộ','90 Phan Chu Trinh, Hải Châu','30.000 đồng - 250.000 đồng')";
        sqLiteDatabase.execSQL(sql1);
        sql1 = "INSERT INTO dsCuahang(anh, ten, mota, diachi, mucgia) VALUES('ch4','Chic Chic - Gà Rán Hàn Quốc','Món ăn vĩa hè ngon chất lượng - độc, lạ','110 Ngô Quyền,ĐN','23.000 đồng - 107.000 đồng')";
        sqLiteDatabase.execSQL(sql1);

        String sql2 = "CREATE TABLE dsSanpham(id INTEGER PRIMARY KEY AUTOINCREMENT, anh text, ten text, cuahang text, gia text, diem text)";
        sqLiteDatabase.execSQL(sql2);
        sql2 = "INSERT INTO dsSanPham(anh, ten, cuahang, gia, diem) VALUES('sp1_1','PhinDi Hạnh Nhân', '1', '24.000đ', '500')," +
                "('sp1_2','PhinDi Kem Sữa', '1', '24.000đ', '100')," +
                "('sp1_3','Cappuccino', '1', '24.000đ', '100')," +
                "('sp1_4','Freeze Trà Xanh', '1', '24.000đ', '100')," +
                "('sp1_5','Trà Sen Vàng', '1', '28.000đ', '100')," +
                "('sp1_6','Trà Thạch Đào', '1', '28.000đ', '100')," +
                "('sp1_7','Chanh Đá Xay', '1', '29.600đ', '100')";
        sqLiteDatabase.execSQL(sql2);
        sql2 = "INSERT INTO dsSanpham(anh, ten, cuahang, gia, diem) VALUES('sp2_1', 'Sữa tươi kem trứng trân châu','2', '163.900đ','10')," +
                "('sp2_2','PhinDi Kem Sữa','2', '108.000đ','10')," +
                "('sp2_3','Matcha sữa trân châu đường đen','2', '60.000đ','10')," +
                "('sp2_4','Sữa chua nếp than','2', '203.000đ','10')," +
                "('sp2_5', 'Sữa xoài đá xay','2', '263.000đ','10')," +
                "('sp2_6', 'Matcha Olong đào','2', '65.000đ','10')," +
                "('sp2_7', 'Hồng trà','2', '94.000đ','10')";
        sqLiteDatabase.execSQL(sql2);
        sql2 = "INSERT INTO dsSanpham(anh, ten, cuahang, gia, diem) VALUES('sp3_1','Bún đậu thập cẩm','3','80.000đ','10')," +
                "('sp3_2','Chân gà sả tắc','3','135.000đ','10')," +
                "('sp3_3','Lẩu riêu cua','3','155.000đ','10')," +
                "('sp3_4','Ram chả cá','3','55.000đ','10')," +
                "('sp3_5','Cút lộn xào me','3','170.000đ','10')," +
                "('sp3_6','Chả ram tôm cuốn cải','3','105.000đ','10')," +
                "('sp3_7','Gỏi khô bò','3','30.000đ','10')";
        sqLiteDatabase.execSQL(sql2);
        sql2 = "INSERT INTO dsSanpham(anh, ten, cuahang, gia, diem) VALUES('sp4_1','Combo Gà BBQ + Com Rong Biển','4','31.000đ','8')," +
                "('sp4_2','Combo Gà BBQ 1/2 con','4','108.000đ','6')," +
                "('sp4_3','Combo Gà Truyền thống + Com Rong Biển','4','31.000đ','5')," +
                "('sp4_4','Trà cam đào','4','17.000đ','5')," +
                "('sp4_5','Combo Gà phô Mai + Com Rong Biển','4','31.000đ','2')," +
                "('sp4_6','Combo Gà phô Mai 1/2 con','4','107.000đ','2')," +
                "('sp4_7','Combo Gà BBQ + Com Rong Biển + nước','4','44.000đ','2')";
        sqLiteDatabase.execSQL(sql2);
    }
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
