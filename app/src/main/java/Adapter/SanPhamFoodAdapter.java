package Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.foody.R;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.List;

public class SanPhamFoodAdapter extends BaseAdapter{

    private Context context;
    private List<SanPhamFood> sanPhamFoodList;

    public SanPhamFoodAdapter(Context context, List<SanPhamFood> sanPhamFoodList) {
        this.context = context;
        this.sanPhamFoodList = sanPhamFoodList;
    }

    @Override
    public int getCount() {
        return sanPhamFoodList.size();
    }

    @Override
    public Object getItem(int i) {
        return sanPhamFoodList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mImg;
        TextView mTitle, mGia, mDiem, mGia2;
        public ViewHolder(View itemView){
            super(itemView);
        }
    }
    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (view == null){
            view = LayoutInflater.from(context).inflate(R.layout.item_sanpham, null);
            holder = new ViewHolder(view);
            holder.mImg = view.findViewById(R.id.img_sp);
            holder.mTitle = view.findViewById(R.id.title_sp);
            holder.mGia = view.findViewById(R.id.gia_sp);
            holder.mDiem = view.findViewById(R.id.diem_sp);
            view.setTag(holder);
        }
        else {
            holder = (ViewHolder) view.getTag();
        }
        try{
            SanPhamFood sp = sanPhamFoodList.get(i);
            String image = sp.getAnh();
            int resId = ((Activity)context).getResources().getIdentifier(image, "drawable", ((Activity)context).getPackageName());
            holder.mImg.setImageResource(resId);
            holder.mTitle.setText(sp.getTen());
            holder.mGia.setText(sp.getGia());
            holder.mDiem.setText(sp.getDiem());

        }catch (Exception ex){
        }
        return view;
    }
}
