package com.example.foody;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import DataBase.CuaHang;

public class ProducDetail extends AppCompatActivity {
    ImageView img, back;
    TextView proName, proName1, proDC, proMGia;
    LinearLayout prodathang;
    CuaHang cuaHang;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        prodathang = findViewById(R.id.dathang_detail);
        prodathang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplication(), SanPhamActivity.class);
                intent.putExtra("id", id);
                startActivity(intent);
            }
        });

        Intent i = getIntent();
        id = i.getIntExtra("id", 0);

        cuaHang = new CuaHang(this);

        proName1 = findViewById(R.id.title_1);
        proName = findViewById(R.id.title_2);
        img = findViewById(R.id.img_detail);
        proDC = findViewById(R.id.dc_detail);
        proMGia = findViewById(R.id.mucgia_detail);

        String selectQuery = "SELECT * FROM dsCuahang WHERE id = '" + id + "'";
        SQLiteDatabase db = cuaHang.getWritableDatabase();
        Cursor cs = db.rawQuery(selectQuery,null);
        if (cs.moveToFirst()){
            do {
                int id = cs.getInt(0);
                String image = cs.getString(1);
                String title = cs.getString(2);
                String des = cs.getString(3);
                String diachi = cs.getString(4);
                String mucgia = cs.getString(5);

                proName.setText(title);
                proName1.setText(title);
                int resId = getResources().getIdentifier(image, "drawable", getPackageName());
                img.setImageResource(resId);
                proDC.setText(diachi);
                proMGia.setText(mucgia);
            }while (cs.moveToNext());
        }
        db.close();

        back = findViewById(R.id.image_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}
